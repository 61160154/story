/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import java.util.Date;
import model.Customer;
import model.Product;
import model.Receipt;
import model.User;

/**
 *
 * @author Admin
 */
public class TestRecipt {

    public static void main(String[] args) {
//        
        Product p1 = new Product(1, "chayen", 30);
        Product p2 = new Product(2, "americano", 40);
        User seller = new User("Naded", "0943250522", "password");
        Customer customer = new Customer("Pawit", "0918905016");
        Receipt receipt = new Receipt(seller, customer);

        receipt.addReceiptDetail(p1, 1);
        receipt.addReceiptDetail(p2, 2);
        System.out.println(receipt);
        receipt.deleteReceiptDetail(0);
        System.out.println(receipt);
        receipt.addReceiptDetail(p1, 1);
        receipt.addReceiptDetail(p2, 2);
        System.out.println(receipt);
    }
}
